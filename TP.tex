\documentclass[12pt,a4paper,NoirEtBlanc]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[french,frenchkw,onelanguage,boxed]{algorithm2e}
\usepackage{exercise}
\usepackage{multicol}
\usepackage{syntax}
\usepackage{bbold}
\usepackage{hyperref}
\def\ExerciseName{Exercice}
\usepackage[left=2.00cm, right=2.00cm, top=2.00cm, bottom=2.00cm]{geometry}
\begin{document}
	\noindent\begin{minipage}{\textwidth}
		\begin{minipage}[t]{10cm}%[c]{\dimexpr0.5\textwidth-0.5\Colsep\relax}
			Modèles de calcul et Validation d'algorithmes\\
			L3 MIAGE, UGA\\
			2019/2020
		\end{minipage}\hfill
	\end{minipage}

	\vspace{1cm}
	\begin{center}
		{\huge Feuille de TP 1 : Initiation à la vérification de programmes.}
	\end{center}

	\begin{Exercise}[title=Préparation de l'environnement de travail]
		On va d'abord préparer l'environnement de travail pour ce TP. 
		On va travailler avec l'outil \emph{Verifast} qui permet de prouver la correction de programmes C (et Java). 

		Les avantages de Verifast sont:
		\begin{itemize}
			\item qu'il est relativement facile à utiliser: les preuves se basent sur des annotations (des commentaires) ajoutées à du C standard;
			\item qu'il marche sur plusieurs plateformes, sans configuration, etc..
		\end{itemize}
		
		Les désavantages de Verifast sont:
		\begin{itemize}
			\item c'est un outil en cours de développement: il peut y avoir des bugs, il n'y a pas beaucoup de bibliothèques, etc.
			\item peu de choses sont inférées, il faut écrire les annotations pour tout ce qu'on veut montrer
		\end{itemize}


		Tout d'abord, se rendre sur \url{https://github.com/verifast/verifast} et téléchargez (et décompressez) le \emph{binaire} qui correspond à la machine, ainsi que le tutoriel.
		Dans le répertoire de l'archive, il y a un sous-repertoire \emph{./bin/} qui contient un executable \emph{vfide}. Lancez cet executable.

		{\bf Important: dans le menu \emph{Verify}, désactiver l'option \emph{Check arithmetic overflow}.}
	\end{Exercise}

	\begin{Exercise}[title=Introduction à l'environnement de travail]
		Lire la section~2 du tutoriel (en cas de difficulté avec l'anglais, demandez) pour voir quelles sont les différentes parties de l'IDE.

		Dans Verifast, chaque fonction explicite les préconditions requises pour pouvoir appeler la fonction, et explicite les postconditions garanties par la fonction.
		L'enjeu de la vérification consiste à montrer que le programme donne les bonnes postconditions lorsqu'on lui donne les bonnes préconditions.

		Chaque fonction est annotée par les préconditions {\bf\tt //@ requires ...} et les postconditions {\bf\tt //@ provides ...}.

		{\bf \emph{Question: Écrire l'en-tête d'une fonction qui incrémente un entier.}}
		L'en-tête doit ressembler à:
		\begin{verbatim}
			int incr(int i) 
			//@requires ...
			//@ensures ...
		\end{verbatim}

		Indice, dans les conditions, on peut utiliser les variables (comme \emph{i} ici). Aussi, dans la postcondition, on peut utiliser la valeur renvoyée sous le nom \emph{result}.

		{\bf \emph{Question: Implémenter le corps de la fonction}}

		Ensuite, appuyez sur la flèche (ou F5) pour tenter de vérifier la fonction.
	\end{Exercise}

	\begin{Exercise}[title=Fonctionnement des boucles]
		Comme on l'a vu au TP0, pour les boucles, on veut montrer deux choses: (i) la boucle termine; et (ii) la boucle renvoie le bon résultat.
		Pour montrer (i), on peut, par exemple, montrer qu'une valeur (entière) diminue à chaque itération, tout en restant positive ou nulle. Cette approche est faite par Vérifast.
		En général, pour (ii), on montre qu'un invariant (une propriété qui ne change pas) est maintenu entre chaque itération de la boucle.

		Dans Verifast, il faut aussi faire les deux étapes, via deux annotations: pour l'invariant, il y a l'annotation {\tt //@ invariant ...} et pour expliciter la une valeur qui décroit, on utilise {\tt //@ decrease ...}.

		{\bf Important, pour montrer que la boucle termine, il y a deux choses: (a) la valeur diminue (qui rentre dans le {\tt //@decrease}) et (b) la valeur est positive à chaque iteration (qui est donc un invariant, et qui rentre dans le {\tt //@ invariant}.}

		{\bf \emph{Question: Implémenter une addition à l'aide d'une boucle et de la fonction {\tt incr}.}}
		La fonction doit ressembler à:
		\begin{verbatim}
		int add(int i, int j) 
		//@requires ... ;
		//@ensures result == i + j; 
		{
			for(...; ... ; ...)
			//@ invariant ...
			//@ decreases ...
			{
			}
			return ...;
		}
\end{verbatim}

		Si besoin, passez par des variables intermédiaires pour écrire vos invariants.

		Indice, pensez bien aux préconditions. Si besoin, faites des hypothèses (par exemple, si vous estimez que vous avez besoin que \(i\) soit pair, rajoutez-le).
	\end{Exercise}

	\begin{Exercise}[title=Lemmes externes]
		En pratique, on ne peut pas tout prouver dans le corps des fonctions: ça serait beaucoup trop gros et répétitif.
		Comme en programmation, on peut sortir certains morceaux de preuves du corps des fonctions.
		On obtient des lemmes: ce sont comme des fonctions, mais qui, au lieu de modifier l'état de notre programme, modifie l'état de notre preuve.

		Par exemple, si je veux montrer (en dehors d'une fonction particulière) que \(i = j \Rightarrow i + 1 = j + 1\), je peux écrire le lemme:
		\begin{verbatim}
		/*@ 
		lemma void incr_eq(int i, int j)
		requires i = j;
		ensures i+1 = j+1;
		{}
		@*/
		\end{verbatim}
		Remarque: dans notre cas particulier, le corps du lemme est vide car Verifast arrive à déduire la conclusion tout seul, mais ce n'est pas toujours le cas.

		Notez bien que tout le lemme est dans des commentaires Verifast ({\tt /*@ ... @*/}). Il est aussi possible d'y mettre dans un fichier séparé avec l'extension {\tt .gh}, et d'include ce fichier avec une annotation {\tt //@ \#include <....gh>}.
	\end{Exercise}
		

	\begin{Exercise}[title=Multiplication rapide]
		Implémentez et prouvez la multiplication rapide vue au TD0.

		Vous aurez besoin de lemmes intermédiaires concernant l'arithmétique avec les modulos.
		Ces lemmes sont fournis ici: \url{https://gitlab.com/rcharron/verifastl3-2020/-/blob/master/lemmas.gh}.

			\begin{algorithm}[H]
				\SetAlgoLined
				\KwIn{$a,b\in\mathbb{N}$}
				\KwOut{$a\times b$}
				$r\gets 0$\;
				\While{$a>0$}{
					\eIf{$a=0\mod2$}{
						$a\gets a/2$\;
					}{
						$r\gets r+b$\;$a\gets (a-1)/2$\;
					}
					$b\gets2b$\;
				}
				\KwRet{$r$}
				
				\caption{Multiplication rapide}
			\end{algorithm}
	\end{Exercise}

	\begin{Exercise}[title=Bonus]
		Si vous avez le temps, vous pouvez réactiver l'option \emph{Verify\(\rightarrow\)Check arithmetic overflow}.
		Cette option, quand elle est activée, vérifie que les opérations sur les entiers ne provoquent pas de dépassement de capacité
			\footnote{Pour rappel, les {\tt int} ne sont pas les entiers \(\mathbb{Z}\), mais \(\mathbb{Z}/n\mathbb{Z}\).} (\emph{overflow} ou \emph{underflow})/
		En désactivant l'option comme on l'a fait au début, on dit à Verifast de ne pas faire de vérification à ce sujet.
		En pratique, on veut éviter les dépassements de capacité (voir par exemple les causes du crash du premier vol d'Ariane V).

		Par exemple, la fonction {\tt incr}, si vous l'avez implémentée intuitivement, peut provoquer un dépassement de capacité.
		Pour pallier à ce problème, il y a deux possibilités: soit modifier la précondition, pour demander explicitement un entier qui ne provoquera pas de dépassement, soit modifier le comportement de la fonction (et donc sa postcondition) pour prendre en compte le cas de dépassement.
	\end{Exercise}


\end{document}
